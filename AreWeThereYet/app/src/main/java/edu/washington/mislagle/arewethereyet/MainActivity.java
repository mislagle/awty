package edu.washington.mislagle.arewethereyet;

import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
        private String message;
        private String number;
        private int minutes;
        private PendingIntent pendingIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final EditText messageInput = (EditText) findViewById(R.id.message);
        final EditText numberInput = (EditText) findViewById(R.id.number);
        final EditText minutesInput = (EditText) findViewById(R.id.minutes);
        final Button start = (Button) findViewById(R.id.start);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message = messageInput.getText().toString();
                number = numberInput.getText().toString();
                try{
                    minutes = Integer.parseInt(minutesInput.getText().toString());
                } catch (Exception e) {
                    minutes = -1;
                }

                if(message == null || message.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please type a message", Toast.LENGTH_SHORT).show();
                } else if(number == null || number.isEmpty() || number.length() != 10){
                    Toast.makeText(getApplicationContext(), "Please supply a valid 10-digit phone number", Toast.LENGTH_SHORT).show();
                } else if(minutes == -1){
                    Toast.makeText(getApplicationContext(), "Please give an interval in whole minutes", Toast.LENGTH_SHORT).show();
                } else if (start.getText().toString().equalsIgnoreCase("start")) {
                    start.setText("STOP");

                    Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
                    alarmIntent.putExtra("message", message);
                    pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, 0);

                    startAlarm();
                } else {
                    start.setText("START");
                    stopAlarm();
                }
            }
        });
    }

    public void startAlarm(){
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = minutes*60000;
        alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(this, "Starting AreWeThereYet", Toast.LENGTH_SHORT).show();
    }

    public void stopAlarm() {
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        Toast.makeText(this, "Stopping AreWeThereYet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        stopAlarm();
    }
}
